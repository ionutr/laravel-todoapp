<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
	'prefix' => 'v1',
	'middleware' => ['guest'],
	'namespace' => 'Api',
	'name' => 'guest.'
], function() {
	Route::post('login', 'AuthController@login')->name('login');
	Route::post('login/refresh', 'AuthController@refresh')->name('api.auth.refresh');
	Route::post('users/register', 'UserController@register')->name('api.users.register');
	Route::post('users/confirm/{guestPrivilege}', 'UserController@confirmEmail')->name('api.users.confirm_email');
});

// AUTH API
Route::group([
	'prefix' => 'v1',
	'middleware' => ['auth:api'],
	'namespace' => 'Api'
], function() {
	Route::get('logout', 'AuthController@logout')->name('api.auth.logout');
	Route::get('users/me', 'UserController@getAuthenticatedUser')->name('api.users.get_auth_user');

	Route::get('todo/types', 'TodoController@getTodoTypes')->name('api.todo.get_todo_types');

	Route::get('todo', 'TodoController@index')->name('api.todo.index');
	Route::get('todo/{todo}/notifications', 'TodoController@getNotifications')->name('api.todo.notifications.index');
	Route::get('todo/{todo}', 'TodoController@show')->name('api.todo.view');
	Route::post('todo', 'TodoController@store')->name('api.todo.create');
	Route::post('todo/{todo}/complete', 'TodoController@completeTodo')->name('api.todo.complete');
	Route::post('todo/{todo}/restore', 'TodoController@restoreTodo')->name('api.todo.restore');
	Route::patch('todo/{todo}/notifications/{notification}', 'TodoController@updateNotification')->name('api.todo.notifications.update');
	Route::post('todo/search', 'TodoController@search')->name('api.todo.search');
	Route::patch('todo/{todo}', 'TodoController@update')->name('api.todo.update');
	Route::delete('todo/{todo}', 'TodoController@destroy')->name('api.todo.delete');
});