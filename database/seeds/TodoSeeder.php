<?php

use Illuminate\Database\Seeder;
use App\Todo;
use App\TodoType;
use Faker\Generator as Faker;
use Carbon\Carbon;

class TodoSeeder extends Seeder
{
    protected $faker;

    function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(Todo::class, 15)->create([
			'user_id' => 1
		]);

        factory(Todo::class, 2)->create([
            'user_id' => 1,
            'type_id' => TodoType::whereName(TodoType::MEETING)->first()->id
        ]);

        factory(Todo::class, 2)->create([
            'user_id' => 1,
            'type_id' => TodoType::whereName(TodoType::CALL)->first()->id
        ]);

        factory(Todo::class, 2)->create([
            'user_id' => 1,
            'type_id' => TodoType::whereName(TodoType::LIFE_EVENT)->first()->id
        ]);

        factory(Todo::class, 3)->create([
            'user_id' => 1,
            'completed_at' => Carbon::now()->subDays($this->faker->numberBetween(1, 7))
        ]);

        factory(Todo::class, 2)->create([
            'user_id' => 1,
            'deadline_at' => Carbon::now()->addDays($this->faker->numberBetween(1, 7))
        ]);
	}
}
