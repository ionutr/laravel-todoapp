<?php

use Illuminate\Database\Seeder;

class TodoTypeSeeder extends Seeder
{
    protected $systemSeeds = [
        [
            'name' => 'Note',
            'user_id' => NULL
        ],
        [
            'name' => 'Meeting',
            'user_id' => NULL
        ],
        [
            'name' => 'Call',
            'user_id' => NULL
        ],
        [
            'name' => 'Life event',
            'user_id' => NULL
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->systemSeeds as $seed) {
            factory(\App\TodoType::class)->create($seed);
        }
    }
}
