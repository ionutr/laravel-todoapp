<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
	const DEFAULT_PASSWORD = '1234';

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(App\User::class)->create([
			'name' => 'Ionuţ Ruş',
			'email' => 'pixelangello@gmail.com',
			'email_verified_at' => now(),
			'password' => Hash::make(self::DEFAULT_PASSWORD)
		]);
	}
}
