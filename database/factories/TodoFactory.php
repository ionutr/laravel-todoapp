<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Todo;
use App\TodoType;
use App\User;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random(1)->first()->id,
        'type_id' => TodoType::whereName(Todo::DEFAULT_TYPE)->first()->id,
	    'title' => $faker->text(30)
    ];
});
