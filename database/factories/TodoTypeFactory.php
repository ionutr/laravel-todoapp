<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TodoType;
use App\User;
use Faker\Generator as Faker;

$factory->define(TodoType::class, function (Faker $faker) {
    return [
        'user_id' => NULL,
        'name' => $faker->colorName
    ];
});
