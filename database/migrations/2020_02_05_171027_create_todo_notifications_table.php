<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->unsignedBigInteger('todo_id');
            $table->timestamps();
            $table->timestamp('scheduled_at')->nullable();
            $table->timestamp('processed_at')->nullable();
            $table->string('type', 128);
            $table->boolean('is_active')->default(true);
            $table->string('title', 128);
            $table->json('params')->nullable();

	        $table->foreign('todo_id')->references('id')->on('todos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo_notifications');
    }
}
