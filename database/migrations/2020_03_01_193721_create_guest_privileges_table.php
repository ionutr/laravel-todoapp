<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestPrivilegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_privileges', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->unsignedBigInteger('context_id');
            $table->timestamps();
	        $table->timestamp('valid_until')->nullable();
            $table->string('context', 128);
            $table->string('token', 32);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_privileges');
    }
}
