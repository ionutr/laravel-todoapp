<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	public static function onSaveAddUserId($model)
	{
		if (
			is_null($model->user_id) &&
			auth()->check()
		) {
			$model->user_id = auth()->id();
		}
	}

	public function saveWithoutEvents()
	{
		static::withoutEvents(function() {
			$this->save();
		});
	}
}