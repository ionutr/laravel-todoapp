<?php

namespace App\Observers;

use App\Events\UserRegistered;
use App\Services\GuestPrivilegeService;
use App\Services\TodoService;
use App\User;

class UserObserver
{
	private $gpService;

	private $todoService;

	public function __construct(GuestPrivilegeService $guestPrivilegeService, TodoService $todoService)
	{
		$this->gpService = $guestPrivilegeService;
		$this->todoService = $todoService;
	}

	/**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
    	// create guestPrivilege
	    $guestPrivilege = $this->gpService->create([
	    	'context_id' => $user->id,
		    'context' => User::class,
		    'valid_until' => now()->addHours(8),
		    'token' => md5($user->id . time())
	    ]);

	    // create welcome note
	    $this->todoService->createWelcomeNote($user);

    	// fire event
        event(new UserRegistered($user, $guestPrivilege));
    }
}
