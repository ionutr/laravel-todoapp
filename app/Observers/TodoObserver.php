<?php

namespace App\Observers;

use App\Events\TodoCreated;
use App\Events\TodoDeadlineSet;
use App\Todo;

class TodoObserver
{
	/**
	 * Handle the todo "creating" event.
	 *
	 * @param  \App\Todo  $todo
	 * @return void
	 */
	public function creating(Todo $todo)
	{
		Todo::onSaveAddUserId($todo);
	}

	/**
	 * Handle the todo "created" event.
	 *
	 * @param  \App\Todo  $todo
	 * @return void
	 */
	public function created(Todo $todo)
	{
		event(new TodoCreated($todo));

		if (!is_null($todo->deadline_at)) {
			event(new TodoDeadlineSet($todo));
		}
	}
}
