<?php

namespace App\Observers;

use App\TodoNotification;
use Carbon\Carbon;

class TodoNotificationObserver
{
    /**
     * Handle the todo notification "saved" event.
     *
     * @param  \App\TodoNotification  $todoNotification
     * @return void
     */
    public function saved(TodoNotification $todoNotification)
    {
    	if (array_key_exists('scheduled_at', $todoNotification->getChanges())) {
		    $notificationClass = $todoNotification->type;
		    $notificationObject = new $notificationClass($todoNotification->todo);
		    if (method_exists($notificationObject, 'setTimeAt')) {
			    $notificationObject->setTimeAt(
				    Carbon::createFromTimestamp(
					    strtotime($todoNotification->scheduled_at)
				    )
			    );
		    }

		    $todoNotification->params = json_encode($notificationObject->getParams());
		    $todoNotification->title = $notificationObject->getTitle();
		    $todoNotification->saveWithoutEvents();
	    }
    }
}
