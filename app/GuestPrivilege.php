<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestPrivilege extends Model
{
    protected $fillable = [
    	'context_id',
	    'valid_until',
	    'context',
	    'token'
    ];

    protected $casts = [
    	'valid_until' => 'datetime'
    ];
}
