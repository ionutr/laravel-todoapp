<?php

namespace App\Services;

/*
 * GuestPrivilegeService class is a service responsible for handling temporary guest privileges
 *
 * @package Services
 * @author  Ionut Rus
 * @version 1.0.0
 */
use App\GuestPrivilege;

class GuestPrivilegeService
{
	public function create(array  $data)
	{
		return GuestPrivilege::create($data);
	}
}