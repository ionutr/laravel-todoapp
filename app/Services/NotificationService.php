<?php

namespace App\Services;

use App\Todo;
use App\Notifications\TodoDeadlineIsOverdue;
use App\TodoNotification;
use Carbon\Carbon;

/*
 * NotificationService class is a service responsible for notifications
 *
 * @package Services
 * @author  Ionut Rus
 * @version 1.0.0
 */
class NotificationService
{
	public function createTodoNotification(array $data = [])
	{
		// calculate title & params
		$notificationClass = $data['type'];
		$todo = Todo::findOrFail($data['todo_id']);
		$notificationObject = new $notificationClass($todo);
		if (method_exists($notificationObject, 'getParams')) {
			$data['params'] = json_encode($notificationObject->getParams());
		}
		$data['title'] = $notificationObject->getTitle();

		$todoNotification = new TodoNotification($data);
		$todoNotification->save();

		return $todoNotification;
	}

	public function notifyAboutOverdueDeadlines()
	{
		foreach (Todo::overdue()->limit(100)->get() as $todo) {
			$todo->user->notify(new TodoDeadlineIsOverdue($todo));
		}
	}

	public function processTodoNotifications()
	{
		foreach (
			TodoNotification::active()
				->unprocessed()
				->scheduledToday()
				->limit(100)
				->get() as $todoNotification
		) {
			// send the notification for scheduled notifications
			$notificationClassName = $todoNotification->type;
			$notificationObject = new $notificationClassName($todoNotification->todo);
			$doSend = true;

			if (method_exists($notificationObject, 'validate')) {
				$doSend = $notificationObject->validate();
			}

			if ($doSend) {
				$todoNotification->todo->user->notify($notificationObject);

				// update to reflect the processing
				$todoNotification->update([
					'processed_at' => Carbon::now()
				]);
			}
		}
	}
}