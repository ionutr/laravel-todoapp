<?php

namespace App\Services;

use App\Exceptions\EmailNotVerifiedException;
use App\Exceptions\InvalidCredentialsException;
use App\Repositories\UserRepository;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Application;

/*
 * AuthService class is a proxy service responsible for OAUTH integration
 *
 * @package Services
 * @author  Ionut Rus
 * @version 1.0.0
 */
class AuthService
{
    /**
     * Refresh token name, used for cookie name
     *
     * @const string
     */
    const REFRESH_TOKEN = 'refreshToken';

    /**
     * Guzzle HTTP instance
     *
     * @var \GuzzleHttp\Client
     * @access private
     */
    private $apiConsumer;

    /**
     * App Auth Manager instance
     *
     * @var \Illuminate\Auth\AuthManager
     */
    private $auth;

    /**
     * App Cookie Jar
     *
     * @var \Illuminate\Cookie\CookieJar
     * @access private
     */
    private $cookie;

    /**
     * App Database Manager
     *
     * @var \Illuminate\Database\DatabaseManager
     * @access private
     */
    private $db;

    /**
     * App Request
     *
     * @var \Illuminate\Http\Request
     * @access private
     */
    private $request;

    /**
     * User Repository
     *
     * @var \App\Repositories\UserRepository
     * @access private
     */
    private $userRepository;

    /**
     * AuthService Constructor, instantiates dependencies
     *
     * @access public
     *
     * @param \Illuminate\Foundation\Application    $app            App class
     * @param \App\Repositories\UserRepository      $userRepository The user repository
     */
    public function __construct(Application $app, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->apiConsumer = new Client([
            'base_uri' => env('APP_PROXY_URL'),
            'defaults' => [
                'exceptions' => false
            ]
        ]);
        $this->auth = $app->make('auth');
        $this->cookie = $app->make('cookie');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @access public
     *
     * @param string $email
     * @param string $password
     *
     * @return array    $response               The response, as array
     * @return string   $response.access_token  The access token
     * @return string   $response.expires_in    Token expiration time
     *
     * @throws \App\Exceptions\InvalidCredentialsException  When auth fails
     */
    public function attemptLogin($email, $password)
    {
        // attempt fetching user via email
        $user = $this->userRepository->getByEmail($email);

        // check user is not null
        if (!is_null($user)) {

	        // check email verified
	        if (!$user->hasVerifiedEmail()) {
		        throw new EmailNotVerifiedException();
	        }

            // send request to proxy
            return $this->proxy('password', [
                'username' => $email,
                'password' => $password
            ]);
        }

        // user not found
        throw new InvalidCredentialsException();
    }

    /**
     * Logout an user, revoking the token
     *
     * @access public
     *
     * @param \App\User $user The user to logout
     */
    public function logout(User $user)
    {
        // fetch access token
    	$accessToken = $user->token();

    	// revoke the token at db level
    	$this->db
		    ->table('oauth_refresh_tokens')
		    ->where('access_token_id', $accessToken->id)
		    ->update([
		    	'revoked' => true
		    ]);
    	$accessToken->revoke();

    	// forget the associated cookie
	    $this->cookie->queue($this->cookie->forget(self::REFRESH_TOKEN));
    }

	/**
	 * Attempt to refresh the access token using a refresh token that has been saved in a cookie
     *
     * @access public
     *
     * @param string $refreshToken The token to be used to attempt refresh. Defaults to NULL,
     * in which case the system will attempt to read it from the request's corresponding cookie
     *
     * @return array    $response               The response, as array
     * @return string   $response.access_token  The access token
     * @return string   $response.expires_in    Token expiration time
     *
     * @throws \App\Exceptions\InvalidCredentialsException  When auth fails
	 */
	public function attemptRefresh($refreshToken = null)
	{
	    if (is_null($refreshToken)) {
	        $refreshToken = $this->request->cookie(self::REFRESH_TOKEN);
        }

		return $this->proxy('refresh_token', [
			'refresh_token' => $refreshToken
		]);
	}

    /**
     * Proxy a request to the OAuth server.
     *
     * @access public
     *
     * @param string    $grantType  what type of grant type should be proxied
     * @param array     $data       the data to send to the server
     *
     * @throws \App\Exceptions\InvalidCredentialsException  When auth fails
     */
    public function proxy($grantType, array $data = [])
    {
    	try {
    	    // attempt to fetch an access token
		    $response = $this->apiConsumer->post('oauth/token', [
			    'form_params' => array_merge($data, [
				    'client_id'     => env('API_CLIENT_ID'),
				    'client_secret' => env('API_CLIENT_SECRET'),
				    'grant_type'    => $grantType
			    ])
		    ]);
	    } catch (\Exception $e) {
		    throw new InvalidCredentialsException();
	    }

	    // decode json response to data
        $data = json_decode($response->getBody());

        // Create a refresh token cookie
        $this->cookie->queue(
            self::REFRESH_TOKEN, // cookie name
            $data->refresh_token, // cookie value
            (int)env('PASSPORT_REFRESH_TOKENS_EXPIRE_IN_HOURS') * 60, // expiry based on PASSPORT_REFRESH_TOKENS_EXPIRE_IN_HOURS
            NULL, // path
            NULL, // domain
            false, // secure
            true // HttpOnly,
        );

        return [
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in
        ];
    }
}
