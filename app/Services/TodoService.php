<?php

namespace App\Services;

use App\Todo;
use App\TodoType;
use App\User;
use Carbon\Carbon;

/*
 * TodoService class is a service responsible for Todo operations
 *
 * @package Services
 * @author  Ionut Rus
 * @version 1.0.0
 */
class TodoService
{
    /**
     * Create a todo
     *
     * @access public
     *
     * @param  array $data The payload to create the todo
     *
     * @return \App\Todo $todo The newly created todo object
     *
     */
	public function create(array $data)
	{
		// default type
		if (!array_key_exists('type_id', $data)) {
			$data['type_id'] = TodoType::note()->id;
		}

		$todo = new Todo($data);
		$todo->save();

		return $todo;
	}

	/**
	 * Create welcome note for new user
	 *
	 * @access public
	 *
	 * @param \App\User $user The new user
	 *
	 * @return void
	 *
	 */
	public function createWelcomeNote(User $user)
	{
		$this->create([
			'user_id' => $user->id,
			'type_id' => TodoType::note()->id,
			'title' => 'Welcome to TodoAPP. Click the arrow to see my description',
			'description' => 'You want to edit me? Double click the sh*t out of me. Browse around, I have some cool features. How about creating a todo for yourself?'
		]);
	}

    /**
     * Update a todo
     *
     * @access public
     *
     * @param \App\Todo $todo   The todo object to be updated
     * @param array     $data   The payload to update the todo
     *
     * @return \App\Todo $todo  The updated todo object
     *
     * @throws \Illuminate\Auth\AuthenticationException                                 When user not authenticated
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException            When targeted todo id not found
     * @throws \Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException When the validation for the payload fails
     */
	public function update(Todo $todo, array $data)
	{
		$todo->update($data);

		return $todo;
	}

    /**
     * Delete a todo
     *
     * @access public
     *
     * @param \App\Todo $todo The todo object to be deleted
     *
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException                                 When user not authenticated
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException            When targeted todo id not found
     */
	public function delete(Todo $todo)
	{
		$todo->delete();
	}

    /**
     * Complete a todo
     *
     * @access public
     *
     * @param \App\Todo $todo The todo object to be completed
     *
     * @return \App\Todo $todo The completed todo object
     *
     * @throws \Illuminate\Auth\AuthenticationException                                 When user not authenticated
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException            When targeted todo id not found
     */
	public function complete(Todo $todo)
	{
		$todo->update([
			'completed_at' => Carbon::now()
		]);

		return $todo;
	}

    /**
     * Revert a completed todo
     *
     * @access public
     *
     * @param \App\Todo $todo The todo object to be restored
     *
     * @return \App\Todo $todo The restored todo object
     *
     * @throws \Illuminate\Auth\AuthenticationException                                 When user not authenticated
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException            When targeted todo id not found
     */
	public function restore(Todo $todo)
	{
		$todo->update([
			'completed_at' => NULL
		]);

		return $todo;
	}
}