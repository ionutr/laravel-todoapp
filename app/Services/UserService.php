<?php

namespace App\Services;

/*
 * UserService class is a service responsible for User operations
 *
 * @package Services
 * @author  Ionut Rus
 * @version 1.0.0
 */
use App\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
	public function create(array $data = [])
	{
		// prepare data
		$data['password'] = Hash::make($data['password']);

		$user = new User($data);
		$user->save();

		return $user;
	}

	public function update(User $user, array $data = [])
	{
		// prepare data
		if (array_key_exists('password', $data)) {
			$data['password'] = md5($data['password']);
		}

		$user->update($data);

		return $user;
	}
}