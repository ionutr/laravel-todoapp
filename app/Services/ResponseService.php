<?php

namespace App\Services;

use App\Exceptions\BadRequestException;

/*
 * ResponseService class is a service responsible for some default API responses
 *
 * @package Services
 * @author  Ionut Rus
 * @version 1.0.0
 */

class ResponseService
{
	public static function getErrorResponse(\Exception $e) {
		$badRequestException = new BadRequestException();

		return response()->json(
			[
				'message' => (env('APP_ENV') === 'production') ? $badRequestException->getMessage() : $e->getMessage()
			],
			$badRequestException->getCode()
		);
	}
}