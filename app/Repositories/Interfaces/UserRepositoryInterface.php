<?php

namespace App\Repositories\Interfaces;

interface UserRepositoryInterface extends BaseRepositoryInteface
{
	public function getById(int $id);

	public function getByEmail($email);
}