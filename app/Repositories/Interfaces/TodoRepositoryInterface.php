<?php

namespace App\Repositories\Interfaces;

interface TodoRepositoryInterface extends BaseRepositoryInteface
{
	public function getById(int $id);

	public function getOwn();
}