<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * Get all of the models from the database.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return User::all();
    }

    /**
     * Get a user by id
     *
     * @param  int  $id
     * @return \App\User
     */
    public function getById(int $id)
    {
        return User::find($id)->get();
    }

    /**
     * Get a user by email
     *
     * @param  string  $email
     * @return \App\User
     */
    public function getByEmail($email)
    {
        return User::whereEmail($email)->first();
    }
}