<?php

namespace App\Repositories;

use App\Http\Requests\TodoSearch;
use App\Repositories\Interfaces\TodoRepositoryInterface;
use App\Todo;
use App\TodoType;
use Carbon\Carbon;

class TodoRepository implements TodoRepositoryInterface
{
	const DEFAULT_PAGINATION = 10;

	const DEFAULT_SORTING = 'created_at';

	const DEFAULT_SORTING_DIRECTION = 'DESC';

	/**
	 * Get all of the models from the database.
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Todo::all();
	}

	/**
	 * Get a model by id
	 *
	 * @param  int  $id
	 * @return \App\Todo
	 */
	public function getById(int $id)
	{
		return Todo::find($id)->get();
	}

	/**
	 * Get todo types from the database
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function getTodoTypes()
	{
		return TodoType::select('id', 'name')->get();
	}

	/**
	 * Get todos belonging to the current logged in user from the database.
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function getOwn()
	{
		return Todo::mine()
			->orderBy('created_at', 'DESC');
	}

	/**
	 * Search todos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function search(TodoSearch $request)
	{
	    // init query
		$query = Todo::mine();

		// before date filter
		$createdAtBeforeFilter = $request->has('created_at_before') ? $request->get('created_at_before') : false;
		if (!!$createdAtBeforeFilter) {
			$query->whereDate('created_at', '<=', Carbon::createFromFormat('Y-m-d H:i:s', $createdAtBeforeFilter)
				->toDateTimeString());
		}

		// after date filter
		$createdAtAfterFilter = $request->has('created_at_after') ? $request->get('created_at_after') : false;
		if (!!$createdAtAfterFilter) {
			$query->whereDate('created_at', '>=', Carbon::createFromFormat('Y-m-d H:i:s', $createdAtAfterFilter)
				->toDateTimeString());
		}

		// pagination param init
		$pagination = $request->has('pagination') ? (int)$request->get('pagination') : TodoRepository::DEFAULT_PAGINATION;
		// order parameters init
		$orderBy = $request->has('orderBy') ? $request->get('orderBy') : TodoRepository::DEFAULT_SORTING;
		$orderDir = $request->has('orderDir') ? $request->get('orderDir') : TodoRepository::DEFAULT_SORTING_DIRECTION;

		// do the ordering
		$query->orderBy($orderBy, $orderDir);

		// paginate the result
		return $query->paginate($pagination);
	}
}