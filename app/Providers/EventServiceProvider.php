<?php

namespace App\Providers;

use App\Events\TodoCreated;
use App\Events\TodoDeadlineSet;
use App\Events\UserRegistered;
use App\Listeners\CreateTodoBasicNotifications;
use App\Listeners\CreateTodoDeadlineNotifications;
use App\Listeners\SendConfirmationEmail;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
		TodoCreated::class => [
			CreateTodoBasicNotifications::class
		],
	    TodoDeadlineSet::class => [
	    	CreateTodoDeadlineNotifications::class
	    ],
	    UserRegistered::class => [
	    	SendConfirmationEmail::class
	    ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
