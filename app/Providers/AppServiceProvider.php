<?php

namespace App\Providers;

use App\Observers\TodoNotificationObserver;
use App\Observers\TodoObserver;
use App\Observers\UserObserver;
use App\Todo;
use App\TodoNotification;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	User::observe(UserObserver::class);
        Todo::observe(TodoObserver::class);
        TodoNotification::observe(TodoNotificationObserver::class);
    }
}
