<?php

namespace App\Exceptions;

class EmailNotVerifiedException extends BaseApiException
{
    protected $code = 401;

    protected $message = 'E-mail not verified yet. Check your inbox and your spam folder.';
}
