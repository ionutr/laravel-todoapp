<?php

namespace App\Exceptions;

use Exception;

class BaseApiException extends Exception
{
    protected $code;

    protected $message;
}
