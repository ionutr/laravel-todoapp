<?php

namespace App\Exceptions;

class BadRequestException extends BaseApiException
{
    protected $code = 400;

    protected $message = 'Bad request';
}
