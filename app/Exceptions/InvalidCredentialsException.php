<?php

namespace App\Exceptions;

class InvalidCredentialsException extends BaseApiException
{
    protected $code = 401;

    protected $message = 'Invalid credentials';
}
