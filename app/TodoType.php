<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoType extends Model
{
    const NOTE = 'Note';

    const MEETING = 'Meeting';

    const CALL = 'Call';

    const LIFE_EVENT = 'Life Event';

    const ALL = [
    	TodoType::NOTE,
    	TodoType::MEETING,
    	TodoType::CALL,
    	TodoType::LIFE_EVENT
    ];

    protected $fillable = [
        'name'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeAllTypeCollection($query, $withName = true)
	{
		return ($withName) ? $query->pluck('name', 'id') : $query->pluck('id');
	}

    public function scopeByUser($query, $id)
    {
        return $query->whereUserId($id);
    }

    public function scopeMeeting($query)
    {
        return $query->whereName(TodoType::MEETING)->first();
    }

    public function scopeNote($query)
    {
        return $query->whereName(TodoType::NOTE)->first();
    }

    public function scopeCall($query)
    {
        return $query->whereName(TodoType::CALL)->first();
    }

    public function scopeLifeEvent($query)
    {
        return $query->whereName(TodoType::LIFE_EVENT)->first();
    }

    public function scopeSystem($query)
    {
        return $query->whereNull('user_id');
    }
}
