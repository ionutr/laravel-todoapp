<?php

namespace App\Http\Requests;


use App\TodoType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TodoPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'title' => 'required|string|max:64',
	        'type_id' => [
	        	'nullable',
		        Rule::in(TodoType::allTypeCollection(false)->all())
	        ],
	        'description' => 'nullable|string',
	        'deadline_at' => 'date_format:Y-m-d H:i:s|nullable'
        ];
    }
}
