<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TodoSearch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_completed' => 'boolean|nullable',
	        'created_at_before' => [
	        	'nullable',
	        	'date_format:Y-m-d H:i:s'
	        ],
	        'created_at_after' => [
		        'nullable',
		        'date_format:Y-m-d H:i:s'
	        ]
        ];
    }
}
