<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class TodoNotificationPatch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
	public function authorize(Request $request)
	{
		return (
			auth()->check() &&
			$request->route('todo')->user_id === auth()->id()
		);
	}

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_active' => 'boolean',
	        'scheduled_at' => 'date_format:Y-m-d H:i:s|nullable'
        ];
    }
}
