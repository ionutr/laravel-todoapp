<?php

namespace App\Http\Requests;

use App\TodoType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TodoPatch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        return (
            auth()->check() &&
            $request->route('todo')->user_id === auth()->id()
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'completed_at' => 'date_format:Y-m-d H:i:s|nullable',
            'deadline_at' => 'date_format:Y-m-d H:i:s|nullable',
	        'type_id' => [
		        'nullable',
		        Rule::in(TodoType::allTypeCollection(false)->all())
	        ],
	        'title' => 'nullable|string|max:64',
	        'description' => 'nullable|string'
        ];
    }
}
