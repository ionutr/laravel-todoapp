<?php

namespace App\Http\Requests;

use App\GuestPrivilege;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EmailConfirmationPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
    	$guestPrivilege = $request->route('guestPrivilege');

        return (
        	!empty($guestPrivilege) &&
	        $guestPrivilege instanceof GuestPrivilege &&
	        $guestPrivilege->context === User::class &&
	        $guestPrivilege->valid_until->gt(now())
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
