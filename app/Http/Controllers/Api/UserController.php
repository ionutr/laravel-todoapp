<?php

namespace App\Http\Controllers\Api;

use App\GuestPrivilege;
use App\Http\Controllers\BaseApiController;
use App\Http\Requests\EmailConfirmationPost;
use App\Http\Requests\UserPost;
use App\Services\UserService;
use App\User;

class UserController extends BaseApiController
{
	private $userService;

	public function __construct(UserService $userService)
	{
		$this->userService = $userService;
	}

	public function confirmEmail(EmailConfirmationPost $request, GuestPrivilege $guestPrivilege)
	{
		// fetch user from GuestPrivilege
		$user = User::find($guestPrivilege->context_id);

		// verify if user already confirmed
		if (!is_null($user->email_verified_at)) {
			abort(400, 'User already confirmed email');
		}

		return response()->json(
			$this->userService->update($user, [
				'email_verified_at' => now()
			])
		);
	}

	public function getAuthenticatedUser()
	{
		return auth('api')->user();
	}

	public function register(UserPost $request)
	{
		return response()->json(
			$this->userService->create($request->validated()),
			201
		);
	}
}
