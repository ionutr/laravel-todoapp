<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseApiController;
use App\Http\Requests\TodoCompletePatch;
use App\Http\Requests\TodoDelete;
use App\Http\Requests\TodoNotificationPatch;
use App\Http\Requests\TodoPatch;
use App\Http\Requests\TodoPost;
use App\Http\Requests\TodoSearch;
use App\Http\Requests\TodoShow;
use App\Http\Resources\TodoNotificationCollection;
use App\Http\Resources\TodoNotificationResource;
use App\Http\Resources\TodoPaginatedResource;
use App\Http\Resources\TodoResource;
use App\Repositories\TodoRepository;
use App\Services\ResponseService;
use App\Services\TodoService;
use App\Todo;
use App\TodoNotification;
use App\TodoType;

class TodoController extends BaseApiController
{
	protected $todoRepository;

	protected $todoService;

	function __construct(TodoService $todoService, TodoRepository $repository)
	{
		$this->todoService = $todoService;
		$this->todoRepository = $repository;
	}

	/**
     * Display a listing of current logged in user's todos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
        	new TodoPaginatedResource(
        		$this->todoRepository->getOwn()
			        ->uncompletedOrRecentlyCompleted()
		            ->paginate(5)
	        )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TodoPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoPost $request)
    {
    	try {
			$todo = $this->todoService->create($request->validated());

		    return response()->json(
			    new TodoResource($todo),
			    201
		    );
	    } catch (\Exception $e) {
		    return ResponseService::getErrorResponse($e);
	    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo                        $todo
     * @param  \App\Http\Requests\TodoShow      $request
     * @return \Illuminate\Http\Response
     */
    public function show(TodoShow $request, Todo $todo)
    {
        return response()->json(new TodoResource($todo));
    }

    public function search(TodoSearch $request)
    {
    	return response()->json(
		    new TodoPaginatedResource($this->todoRepository->search($request))
	    );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TodoPatch     $request
     * @param  \App\Todo                        $todo
     * @return \Illuminate\Http\Response
     */
    public function update(TodoPatch $request, Todo $todo)
    {
        return response()->json($this->todoService->update(
        	$todo,
	        $request->validated()
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Requests\TodoDelete        $request
     * @param  \App\Todo                            $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(TodoDelete $request, Todo $todo)
    {
    	$this->todoService->delete($todo);

        return response()->json(NULL, 204);
    }

	/**
	 * Complete a todo
	 *
	 * @param  \App\Http\Requests\TodoCompletePatch     $request
	 * @param  \App\Todo                                $todo
	 * @return \Illuminate\Http\Response
	 */
    public function completeTodo(TodoCompletePatch $request, Todo $todo)
    {
	    return response()->json($this->todoService->complete($todo));
    }

	/**
	 * Restore a todo
	 *
	 * @param  \App\Http\Requests\TodoCompletePatch     $request
	 * @param  \App\Todo                                $todo
	 * @return \Illuminate\Http\Response
	 */
	public function restoreTodo(TodoCompletePatch $request, Todo $todo)
	{
		return response()->json($this->todoService->restore($todo));
	}

	/**
	 * Display a listing of todo types
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getTodoTypes()
	{
		return response()->json(
			$this->todoRepository->getTodoTypes()
		);
	}

	/**
	 * Display a listing of todo notifications
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getNotifications(TodoShow $request, Todo $todo)
	{
		return response()->json(
			new TodoNotificationCollection(
				TodoNotification::byTodoId($todo->id)->get()
			)
		);
	}

	public function updateNotification(TodoNotificationPatch $request, Todo $todo, TodoNotification $notification)
	{
		$notification->update($request->validated());

		return response()->json(
			new TodoNotificationResource($notification)
		);
	}
}
