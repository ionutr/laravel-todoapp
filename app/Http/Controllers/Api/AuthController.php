<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InvalidCredentialsException;
use App\Http\Controllers\BaseApiController;
use App\Http\Requests\LoginPost;
use App\Services\AuthService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;

class AuthController extends BaseApiController
{
    private $authProxy;

    public function __construct(AuthService $authService)
    {
        $this->authProxy = $authService;
    }

    /**
	 * Login user and create token
	 *
	 *@param [LoginPost] request
	 */
	public function login(LoginPost $request)
	{
		try {
			$response = $this->authProxy->attemptLogin(
				$request->get('email'),
				$request->get('password')
			);
		} catch (\Exception $e) {
			return response()->json([
				'message' => $e->getMessage()
			], $e->getCode());
		}

        return response()->json($response);
	}

	public function refresh(Request $request)
	{
		return response()->json(
			$this->authProxy->attemptRefresh()
		);
	}

	/**
	 * Logout user(Revoke the tokens)
	 *
	 * @return [string] message
	 */
	public function logout()
	{
		$this->authProxy->logout(request()->user());

		return response()->json([
			'message' => 'Successfully logged out'
		], 200);
	}
}
