<?php

namespace App\Http\Controllers;

use App\TodoType;
use Illuminate\Http\Request;

class TodoTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TodoType  $todoType
     * @return \Illuminate\Http\Response
     */
    public function show(TodoType $todoType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TodoType  $todoType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TodoType $todoType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TodoType  $todoType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TodoType $todoType)
    {
        //
    }
}
