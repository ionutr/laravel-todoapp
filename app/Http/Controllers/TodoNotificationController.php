<?php

namespace App\Http\Controllers;

use App\TodoNotification;
use Illuminate\Http\Request;

class TodoNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TodoNotification  $todoNotification
     * @return \Illuminate\Http\Response
     */
    public function show(TodoNotification $todoNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TodoNotification  $todoNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(TodoNotification $todoNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TodoNotification  $todoNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TodoNotification $todoNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TodoNotification  $todoNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(TodoNotification $todoNotification)
    {
        //
    }
}
