<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TodoNotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    	$data = [
    		'id' => $this->id,
		    'scheduled_at' => $this->scheduled_at,
		    'processed_at' => $this->processed_at,
		    'type' => $this->type,
		    'is_active' => $this->is_active,
		    'title' => $this->title
	    ];

        return $data;
    }
}
