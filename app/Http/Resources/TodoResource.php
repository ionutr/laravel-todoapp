<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TodoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    	$data = [
		    'id' => $this->id,
		    'created_at' => $this->created_at,
		    'updated_at' => $this->updated_at,
		    'completed_at' => $this->completed_at,
		    'deadline_at' => $this->deadline_at,
		    'title' => $this->title,
		    'description' => $this->description,
		    'is_overdue' => $this->getIsOverdueAttribute()
	    ];

    	if ($request->has('withType')) {
    		$data['type'] = $this->type;
	    } else {
		    $data['type_id'] = $this->type->id;
	    }

        return $data;
    }
}
