<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TodoPaginatedResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$paginationData = $this->resource->toArray(); //@TODO: this feels hacky
		unset($paginationData['data']);

        return [
        	'data' => $this->collection->transform(function($todo) {
		        return new TodoResource($todo);
	        })->toArray(),
	        'pagination' => $paginationData
        ];
    }
}
