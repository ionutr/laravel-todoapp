<?php

namespace App\Http\Middleware;

use App\Exceptions\BaseApiException;
use App\Services\AuthService;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate extends Middleware
{
	protected $authService;

	/**
	 * Create a new middleware instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Factory  $auth
	 * @return void
	 */
	public function __construct(Auth $auth, AuthService $authService)
	{
		$this->auth = $auth;
		$this->authService = $authService;

		parent::__construct($auth);
	}

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string[]  ...$guards
	 * @return mixed
	 *
	 * @throws \Illuminate\Auth\AuthenticationException
	 */
	public function handle($request, Closure $next, ...$guards)
	{
		try {
			$this->authenticate($request, $guards);
		} catch (\Exception $e) {
			if ($request->hasCookie(AuthService::REFRESH_TOKEN)) {
				try {
					$data = $this->authService->attemptRefresh();
					if (
						is_array($data) &&
						!empty($data) &&
						array_key_exists('access_token', $data)
					) {
						$request->headers->set('Authorization', 'Bearer ' . $data['access_token']);

						try {
							$this->authenticate($request, $guards);
						} catch (\Exception $e) {
							throw new AuthenticationException(
								'Unauthenticated.', $guards, NULL
							);
						}
					}
				} catch (\Exception $e) {
					throw new AuthenticationException(
						'Unauthenticated.', $guards, NULL
					);
				}
			} else {
				throw new AuthenticationException(
					'Unauthenticated.', $guards, NULL
				);
			}
		}

		return $next($request);
	}
}
