<?php

namespace App\Notifications;

use App\Todo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TodoDeadlineToday extends Notification
{
    use Queueable;

	private $todo;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Todo $todo)
	{
		$this->todo = $todo;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return [
			'mail',
			'database'
		];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->subject('Deadline today')
			->greeting($this->todo->title)
			->salutation(env('APP'))
			->line('You have a todo created ' . $this->todo->created_at->diffForHumans() .  ' that has a deadline today.')
			->action('View todo', env('APP_FRONTEND_URL') . "/edit/{$this->todo->id}");
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			'todo' => $this->todo->toArray()
		];
	}

	public function getTitle(): string
	{
		return 'Notify me on the day of the due deadline.';
	}

	public function validate()
	{
		return is_null($this->todo->completed_at);
	}
}
