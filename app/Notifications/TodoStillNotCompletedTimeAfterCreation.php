<?php

namespace App\Notifications;

use App\Todo;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TodoStillNotCompletedTimeAfterCreation extends Notification
{
    use Queueable;

    private $timeAt;

    private $todo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Todo $todo)
    {
        $this->todo = $todo;
	    $this->timeAt = $this->todo->created_at->addWeek(1);
    }

    public function setTimeAt($timeAt)
    {
    	$this->timeAt = $timeAt;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
	    return [
		    'mail',
		    'database'
	    ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
	        ->subject('A todo on your list requires attention')
	        ->greeting($this->todo->title)
	        ->salutation(env('APP'))
	        ->line('You have a todo created ' . $this->todo->created_at->diffForHumans() .  ' that is still not completed.')
	        ->action('View todo', env('APP_FRONTEND_URL') . "/edit/{$this->todo->id}");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
	        'todo' => $this->todo->toArray()
        ];
    }

    public function getParams(): array
    {
    	return [
    		'%time%' => $this->timeAt
	    ];
    }

    public function getTitle(): string
    {
    	$formatString = '';
    	$diff = $this->getParams()['%time%']->diff($this->todo->created_at);

    	if ($diff->y > 0) {
    		$formatString .= '%y years, ';
	    }
	    if ($diff->m > 0) {
		    $formatString .= '%m months, ';
	    }
	    if ($diff->d > 0) {
		    $formatString .= '%d days, ';
	    }
	    if ($diff->h > 0) {
		    $formatString .= '%h hours, ';
	    }
	    if (
	        (
	        	$diff->y < 1 &&
		        $diff->m < 1 &&
		        $diff->h < 1
	        ) &&
	    	$diff->i > 0
	    ) {
		    $formatString .= '%i minutes, ';
	    }
	    $formatString = substr($formatString, 0, -2) . ' after';

		return str_replace(
			'%time%',
			$diff->format($formatString),
			$this->getTitleTemplate()
		);
    }

	public function getTitleTemplate(): string
	{
		return 'Notify me %time% todo was created, if not completed yet.';
	}

	public function validate(): boolean
	{
		return is_null($this->todo->completed_at);
	}
}
