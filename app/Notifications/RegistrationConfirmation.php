<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegistrationConfirmation extends Notification
{
    use Queueable;

    private $token;

    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
	    return [
		    'mail',
		    'database'
	    ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
	    return (new MailMessage)
		    ->subject('Confirm your registration')
		    ->greeting('Just one more click to complete registration')
		    ->salutation(env('APP'))
		    ->line('Click on the button below to confirm your registration to TodoAPP.')
		    ->action('Confirm registration', env('APP_FRONTEND_URL') . "/confirm/{$this->token}");
    }

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable): array
	{
		return [
			'user' => $this->user->toArray(),
			'token' => $this->token
		];
	}
}
