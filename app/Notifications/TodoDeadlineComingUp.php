<?php

namespace App\Notifications;

use App\Todo;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TodoDeadlineComingUp extends Notification
{
    use Queueable;

    private $timeAt;

	private $todo;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Todo $todo)
	{
		$this->todo = $todo;
		$this->timeAt = Carbon::createFromTimestamp(strtotime($this->todo->deadline_at))->subDay();
	}

	public function setTimeAt($timeAt)
	{
		$this->timeAt = $timeAt;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable): array
	{
		return [
			'mail',
			'database'
		];
	}

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
	    return (new MailMessage)
		    ->subject('Deadline coming up')
		    ->greeting($this->todo->title)
		    ->salutation(env('APP'))
		    ->line('You have a todo created ' . $this->todo->created_at->diffForHumans() .  ' that has a deadline coming up tomorrow.')
		    ->action('View todo', env('APP_FRONTEND_URL') . "/edit/{$this->todo->id}");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
	    return [
		    'todo' => $this->todo->toArray()
	    ];
    }

	public function getParams(): array
	{
		return [
			'%time%' => $this->timeAt
		];
	}

	public function getTitle(): string
	{
		return str_replace(
			'%time%',
			$this->getParams()['%time%']->diffForHumans($this->todo->deadline_at),
			$this->getTitleTemplate()
		);
	}

	public function getTitleTemplate(): string
	{
		return 'Notify me %time% before todo is due.';
	}

	public function validate(): boolean
	{
		return is_null($this->todo->completed_at);
	}
}
