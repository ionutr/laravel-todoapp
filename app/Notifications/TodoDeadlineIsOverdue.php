<?php

namespace App\Notifications;

use App\Todo;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TodoDeadlineIsOverdue extends Notification implements ShouldQueue
{
    use Queueable;

    private $todo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Todo $todo)
    {
        $this->todo = $todo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
        	'mail',
	        'database'
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
	                ->greeting($this->todo->title)
	                ->salutation('Todo APP')
                    ->line('You have an overdue todo. Deadline was ' . Carbon::createFromTimeStamp(strtotime
	                    ($this->todo->deadline_at))->diffForHumans() . ", on " . $this->todo->deadline_at)
	                ->action('View todo', env('APP_FRONTEND_URL') . "/edit/{$this->todo->id}");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'todo' => $this->todo->toArray()
        ];
    }
}
