<?php

namespace App\Listeners;

use App\Events\TodoDeadlineSet;
use App\Notifications\TodoStillNotCompletedTimeAfterCreation;
use App\Services\NotificationService;
use App\TodoNotification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateTodoDeadlineNotifications implements ShouldQueue
{
    use InteractsWithQueue;

    private $service;

    function __construct(NotificationService $notificationService)
    {
    	$this->service = $notificationService;
    }

	/**
     * Handle the event.
     *
     * @param  TodoDeadlineSet  $event
     * @return void
     */
    public function handle(TodoDeadlineSet $event)
    {
		// Create notification #1: Notify me one day before todo is due.
	    $this->service->createTodoNotification([
		    'todo_id' => $event->todo->id,
		    'scheduled_at' => Carbon::createFromTimestamp(strtotime($event->todo->deadline_at))->subDay(),
		    'type' => '\App\Notifications\TodoDeadlineComingUp'
	    ]);

	    // Create notification #2: Notify me on the day of the due deadline.
	    $this->service->createTodoNotification([
		    'todo_id' => $event->todo->id,
		    'scheduled_at' => $event->todo->deadline_at,
		    'type' => '\App\Notifications\TodoDeadlineToday'
	    ]);
    }
}
