<?php

namespace App\Listeners;

use App\Events\TodoCreated;
use App\Services\NotificationService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateTodoBasicNotifications implements ShouldQueue
{
    use InteractsWithQueue;

	private $service;

	function __construct(NotificationService $notificationService)
	{
		$this->service = $notificationService;
	}

    /**
     * Handle the event.
     *
     * @param  TodoCreated  $event
     * @return void
     */
    public function handle(TodoCreated $event)
    {
	    // create #1 notification: Notify me one week after todo was created, if not completed yet.
	    $this->service->createTodoNotification([
		    'todo_id' => $event->todo->id,
		    'scheduled_at' => $event->todo->created_at->addWeek(),
		    'type' => '\App\Notifications\TodoStillNotCompletedTimeAfterCreation'
	    ]);
    }
}
