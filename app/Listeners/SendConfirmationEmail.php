<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Notifications\RegistrationConfirmation;
use App\Services\NotificationService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendConfirmationEmail
{
	use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        // send notification
	    $event->user->notify(new RegistrationConfirmation($event->user, $event->privilege->token));
    }
}
