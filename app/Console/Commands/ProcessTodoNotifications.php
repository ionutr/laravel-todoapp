<?php

namespace App\Console\Commands;

use App\Services\NotificationService;
use Illuminate\Console\Command;

class ProcessTodoNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'todo:process-todo-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

	private $service;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(NotificationService $notificationService)
	{
		parent::__construct();

		$this->service = $notificationService;
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->service->processTodoNotifications();
    }
}
