<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Todo extends BaseModel
{
    const DEFAULT_TYPE = 'note';

    protected $fillable = [
    	'user_id',
        'type_id',
    	'completed_at',
    	'deadline_at',
	    'title',
	    'description'
    ];

    protected static function boot()
    {
	    parent::boot();

//	    // pre save hook
//	    self::creating(function($todo) {
//		    Todo::onSaveAddUserId($todo);
//		    dd('aaa');
//		    if (!is_null($todo->deadline_at)) {
//		    	// create default notifications
//		    }
//	    });
    }

	public function type()
    {
        return $this->belongsTo(TodoType::class, 'type_id', 'id');
    }

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function scopeMine($query)
	{
		return $query->whereUserId(auth()->id());
	}

	public function scopeNotes($query)
    {
        return $query->whereTypeId(TodoType::whereName(TodoType::NOTE)->first()->id);
    }

    public function scopeMeetings($query)
    {
        return $query->whereTypeId(TodoType::whereName(TodoType::MEETING)->first()->id);
    }

    public function scopeCalls($query)
    {
        return $query->whereTypeId(TodoType::whereName(TodoType::CALL)->first()->id);
    }

    public function scopeLifeEvents($query)
    {
        return $query->whereTypeId(TodoType::whereName(TodoType::LIFE_EVENT)->first()->id);
    }

	public function scopeCompleted($query)
    {
        return $query->whereNotNull('completed_at');
    }

	public function scopeUncompleted($query)
	{
		return $query->whereNull('completed_at');
	}

	public function scopeHasDeadline($query)
    {
        return $query->whereNotNull('deadline_at');
    }

	public function scopeOverdue($query)
    {
        return $query->whereDate('deadline_at', '<', Carbon::now())
	        ->whereNull('completed_at');
    }

	public function scopeUncompletedOrRecentlyCompleted($query)
	{
		return $query->whereNull('completed_at')
			->orWhereDate('completed_at', '=', Carbon::today()->toDateString());
	}

	public function getIsOverdueAttribute()
    {
        return (
            !is_null($this->deadline_at) &&
            Carbon::now()->gt($this->deadline_at) &&
            is_null($this->completed_at)
        );
    }
}
