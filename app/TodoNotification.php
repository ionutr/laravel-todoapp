<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TodoNotification extends BaseModel
{
    protected $fillable = [
    	'todo_id',
	    'scheduled_at',
	    'processed_at',
	    'type',
	    'is_active',
	    'title'
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $observables = [
    	'scheduled_at_changed'
    ];

	public function todo()
	{
		return $this->belongsTo(Todo::class, 'todo_id', 'id');
	}

	public function scopeActive($query)
	{
		return $query->where('is_active', true);
	}

	public function scopeByTodoId($query, $todoId)
	{
		return $query->where('todo_id', $todoId);
	}

	public function scopeUnprocessed($query)
	{
		return $query->whereNull('processed_at');
	}

	public function scopeScheduledToday($query)
	{
		return $query->whereDate('scheduled_at', '=', Carbon::today()->toDateString());
	}
}
