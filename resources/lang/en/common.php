<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Common Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'back_to_app' => 'Back to app',
	'sign_up' => 'Sign up',
	'sign_out' => 'Sign out',
	'load_x_more' => 'Load :x more',
	'save' => 'Save',
	'update' => 'Update',
	'cancel' => 'Cancel',
	'undo' => 'Undo',
	'last_updated' => 'Last updated',
	'confirmation' => 'Confirmation'
];
