<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Members Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'register' => 'Register',
	'register_success' => 'Successfully registered user :user. Please check your inbox to complete the process.',
	'complete_registration' => 'Complete registration',
];
