<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Webapp Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'form_add_todo_placeholder_title' => 'Todo title',
	'form_add_todo_placeholder_description' => 'Todo description(optional)',
	'form_add_todo_title_add_deadline' => 'Add a deadline',
	'form_add_todo_title_configure_notifications' => 'Configure notifications',
	'form_add_todo_title_add_description' => 'Add a text description',
	'successfully_created_todo' => 'Successfully created todo',
	'successfully_updated_todo' => 'Successfully updated todo',
	'successfully_restored_todo' => 'Successfully uncompleted todo',
	'nav_todo_list' => 'Todo list',
	'nav_more' => 'More',
	'nav_about' => 'About this app',
	'nav_author' => 'About the author',
	'nav_report' => 'Report an issue',
	'title_add_todo' => 'Add a todo',
	'title_change_todo_type' => 'Change todo type',
	'todo_type_note' => 'Note',
	'todo_type_meeting' => 'Meeting',
	'todo_type_call' => 'Call',
	'todo_type_life_event' => 'Life event',
];
