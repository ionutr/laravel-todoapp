<?php

namespace Tests\Unit;

use App\Todo;
use App\TodoType;
use Carbon\Carbon;
use Tests\TestCase;

/*
 * TodoService unit tests
 *
 * @package Tests\Unit
 * @author  Ionut Rus
 * @version 1.0.0
 */
class TodoServiceTest extends TestCase
{
    /**
     * Request
     *
     * @var \Illuminate\Http\Request
     * @access private
     */
    private $request;

    /**
     * Todo Service
     *
     * @var \App\Services\TodoService
     * @access private
     */
    private $todoService;

    /**
     * Setup the test
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->request = $this->app->make('request');
        $this->todoService = $this->app->make('App\Services\TodoService');
    }

    /**
     * Test that create() method works
     *
     * @return void
     */
    public function testCreateWorks()
    {
        // create a test user
        $user = factory('App\User')->create();

        // attempt to create the todo
        try {
            $todo = $this->todoService->create([
                'user_id' => $user->id,
                'type_id' => TodoType::note()->id,
                'title' => 'A note title',
                'description' => 'The description of the note'
            ]);

            // check created todo
            $this->assertNotNull($todo);
            // check data
            $this->assertEquals($user->id, $todo->user_id);
            $this->assertEquals(TodoType::note()->id, $todo->type_id);
            $this->assertEquals('A note title', $todo->title);
            $this->assertEquals('The description of the note', $todo->description);
            // check object type
            $this->assertTrue(get_class($todo) == 'App\Todo', 'Wrong object type created');
        } catch (\Exception $exception) {
            $this->assertTrue(false, $exception->getMessage());
        }
    }

    /**
     * Test that update() method works
     *
     * @return void
     */
    public function testUpdateWorks()
    {
        // create a test user
        $user = factory('App\User')->create();

        // create a todo
        $todo = $this->todoService->create([
            'user_id' => $user->id,
            'type_id' => TodoType::note()->id,
            'title' => 'A note title',
            'description' => 'The description of the note'
        ]);

        // init payload
        $payload = [
            'title' => 'A changed note title'
        ];

        try {
            // attempt the update
            $todo = $this->todoService->update($todo, $payload);
            // check updated todo
            $this->assertNotNull($todo);
            // check data
            $this->assertEquals($user->id, $todo->user_id);
            $this->assertEquals($todo->title, $payload['title']);
        } catch (\Exception $exception) {
            $this->assertTrue(false, $exception->getMessage());
        }
    }

    /**
     * Test that delete() method works
     *
     * @return void
     */
    public function testDeleteWorks()
    {
        // create a test user
        $user = factory('App\User')->create();

        // create a todo
        $todo = $this->todoService->create([
            'user_id' => $user->id,
            'type_id' => TodoType::note()->id,
            'title' => 'A note title',
            'description' => 'The description of the note'
        ]);

        try {
            // attempt the update
            $todoId = $todo->id;
            $this->todoService->delete($todo);

            // attempt to find todo by previous id
            $todo = Todo::find($todoId);

            // check deleted todo
            $this->assertNull($todo);
        } catch (\Exception $exception) {
            $this->assertTrue(false, $exception->getMessage());
        }
    }

    /**
     * Test that complete() method works
     *
     * @return void
     */
    public function testCompleteWorks()
    {
        // create a test user
        $user = factory('App\User')->create();

        // create a todo
        $todo = $this->todoService->create([
            'user_id' => $user->id,
            'type_id' => TodoType::note()->id,
            'title' => 'A note title',
            'description' => 'The description of the note'
        ]);

        try {
            // check todo not completed yet
            $this->assertNull($todo->completed_at);

            // complete the todo
            $this->todoService->complete($todo);

            // refresh todo
            $todo->refresh();

            // check completed todo
            $this->assertNotNull($todo->completed_at);
        } catch (\Exception $exception) {
            $this->assertTrue(false, $exception->getMessage());
        }
    }

    /**
     * Test that restore() method works
     *
     * @return void
     */
    public function testRestoreWorks()
    {
        // create a test user
        $user = factory('App\User')->create();

        // create a  ompleted todo
        $todo = $this->todoService->create([
            'user_id' => $user->id,
            'type_id' => TodoType::note()->id,
            'title' => 'A note title',
            'description' => 'The description of the note',
            'completed_at' => Carbon::now()
        ]);

        try {
            // restore the todo
            $this->todoService->restore($todo);

            // refresh todo
            $todo->refresh();

            // check completed todo
            $this->assertNull($todo->completed_at);
        } catch (\Exception $exception) {
            $this->assertTrue(false, $exception->getMessage());
        }
    }
}
