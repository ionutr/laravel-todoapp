<?php

namespace Tests\Unit;

use App\Exceptions\InvalidCredentialsException;
use App\Services\AuthService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/*
 * AuthService unit tests
 *
 * @package Tests\Unit
 * @author  Ionut Rus
 * @version 1.0.0
 */
class AuthServiceTest extends TestCase
{
    /**
     * Auth Service
     *
     * @var \App\Services\AuthService
     * @access private
     */
    private $authService;

    /**
     * Cookie Jar Service
     *
     * @var \\Illuminate\Cookie\CookieJar
     * @access private
     */
    private $cookieService;

    /**
     * Setup the test
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->authService = $this->app->make('App\Services\AuthService');
        $this->cookieService = $this->app->make('cookie');
    }

    /**
     * Test the attemptLogin method succeeds with valid credentials
     *
     * @return void
     */
    public function testAttemptLoginWorks()
    {
        // create a test user
        $user = factory('App\User')->create();

        // attempt the login, fetch the response
        try {
            $response = $this->authService->attemptLogin($user->email, 'password');

            // check response keys
            $this->assertArrayHasKey('access_token', $response);
            $this->assertArrayHasKey('expires_in', $response);
            // check refresh token cookie present
            $this->assertTrue($this->cookieService->hasQueued(AuthService::REFRESH_TOKEN), 'The refresh token appears to not be set/queued');
        } catch (InvalidCredentialsException $exception) {
            $this->assertTrue(false, 'Login failed');
        }
    }

    /**
     * Test the attemptLogin method fails with invalid credentials
     *
     * @return void
     */
    public function testAttemptLoginFails()
    {
        // create a test user
        $user = factory('App\User')->create();

        // attempt the login, fetch the response
        try {
            $response = $this->authService->attemptLogin(
                $user->email,
                'invalid-password'
            );

            // code should not get here
            $this->assertNull($response, 'The login should have failed here');
        } catch (InvalidCredentialsException $exception) {
            $this->assertTrue(true);
        }
    }

    /**
     * Test the logout method works
     *
     * @return void
     */
    public function testLogoutWorks()
    {
        // create a test user
        $user = factory('App\User')->create();

        // attempt the login, fetch the response
        $this->authService->attemptLogin($user->email, 'password');

        $token = $user->createToken('Test');
        $user->withAccessToken($token->token);

        // attempt logout
        $this->authService->logout($user);

        // check refresh token is gone or null
        $this->assertTrue((
            !$this->cookieService->hasQueued(AuthService::REFRESH_TOKEN) ||
            is_null($this->cookieService->queued(AuthService::REFRESH_TOKEN)->getValue())
        ), 'The refresh token seems to be still present and not nullified');
    }

    public function testAttemptRefresh()
    {
        // create a test user
        $user = factory('App\User')->create();

        // login first, to have the refresh token cookie set
        $this->authService->attemptLogin($user->email, 'password');

        // attempt the refresh, fetch response
        try {
            $response = $this->authService->attemptRefresh($this->cookieService->queued(AuthService::REFRESH_TOKEN)->getValue());

            // check response keys present
            $this->assertArrayHasKey('access_token', $response);
            $this->assertArrayHasKey('expires_in', $response);
        } catch (InvalidCredentialsException $exception) {
            $this->assertTrue(false, 'Refresh token failed');
        }
    }
}
